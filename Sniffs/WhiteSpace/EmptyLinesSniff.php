<?php
/**
 * \Drupal\Sniffs\WhiteSpace\EmptyLinesSniff.
 *
 * @category PHP
 * @package  PHP_CodeSniffer
 * @link     http://pear.php.net/package/PHP_CodeSniffer
 */

namespace PHP_CodeSniffer\Standards\Dsn\Sniffs\WhiteSpace;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

/**
 * \Drupal\Sniffs\WhiteSpace\EmptyLinesSniff.
 *
 * Checks that there are not more than x empty lines following each other.
 *
 * @category PHP
 * @package  PHP_CodeSniffer
 * @link     http://pear.php.net/package/PHP_CodeSniffer
 */
class EmptyLinesSniff implements Sniff
{
	/**
     * The number of lines should be empty lines following each other.
     *
     * @var integer
     */
    public $lines = 1;


    /**
     * A list of tokenizers this sniff supports.
     *
     * @var array<string>
     */
    public $supportedTokenizers = [
        'PHP',
        'JS',
        'CSS',
    ];


    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array<int|string>
     */
    public function register()
    {
        return [T_WHITESPACE];

    }//end register()


    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The file being scanned.
     * @param int                         $stackPtr  The position of the current token
     *                                               in the stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        if ($this->lines < 1) {
        	return;
        }

        $n = $this->lines + 1;
        $outLimit = true;
        if ($tokens[$stackPtr]['column'] == 1 && $tokens[$stackPtr]['content'] === $phpcsFile->eolChar) {
            for ($i = 1; $i < $n; $i++) {
                if (!isset($tokens[($stackPtr + $i)]) || 
                    (isset($tokens[($stackPtr + $i)]) && 
                     $tokens[($stackPtr + $i)]['column']==1 && 
					 $tokens[($stackPtr + $i)]['content'] !== $phpcsFile->eolChar)
				) {
                    $outLimit = false;
                    break;
                }
            }


            if ($outLimit === true) {
                $error = 'More than ' . $this->lines . ' empty lines are not allowed';
                $phpcsFile->addError($error, ($stackPtr + $this->lines), 'EmptyLines');
            }
        }

    }//end process()


}//end class
