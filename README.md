## About
Standarts for PHP_CodeSniffer (phpcs)


## The Dsn sniffs
`phpcs --standard=Dsn -e`

```
The Dsn standard contains 45 sniffs

Dsn (5 sniffs)
--------------
  Dsn.Arrays.Array
  Dsn.ControlStructures.SwitchDeclaration
  Dsn.NamingConventions.ValidFunctionName
  Dsn.NamingConventions.ValidVariableName
  Dsn.WhiteSpace.EmptyLinesSniff
  Dsn.WhiteSpace.IfEmptyLinesSniff

Generic (18 sniffs)
-------------------
  Generic.Classes.OpeningBraceSameLine
  Generic.CodeAnalysis.EmptyStatement
  Generic.CodeAnalysis.UnusedFunctionParameter
  Generic.Commenting.Fixme
  Generic.Commenting.Todo
  Generic.ControlStructures.InlineControlStructure
  Generic.Files.LineEndings
  Generic.Formatting.SpaceAfterCast
  Generic.Functions.FunctionCallArgumentSpacing
  Generic.Functions.OpeningFunctionBraceKernighanRitchie
  Generic.NamingConventions.UpperCaseConstantName
  Generic.PHP.BacktickOperator
  Generic.PHP.CharacterBeforePHPOpeningTag
  Generic.PHP.DisallowShortOpenTag
  Generic.PHP.LowerCaseConstant
  Generic.PHP.LowerCaseKeyword
  Generic.PHP.LowerCaseType
  Generic.WhiteSpace.DisallowSpaceIndent

PEAR (1 sniff)
---------------
  PEAR.Functions.ValidDefaultValue

PSR12 (1 sniff)
----------------
  PSR12.Operators.OperatorSpacing

PSR2 (6 sniffs)
---------------
  PSR2.ControlStructures.ElseIfDeclaration
  PSR2.Files.ClosingTag
  PSR2.Files.EndFileNewline
  PSR2.Methods.FunctionCallSignature
  PSR2.Methods.FunctionClosingBrace
  PSR2.Methods.MethodDeclaration

Squiz (13 sniffs)
-----------------
  Squiz.ControlStructures.ControlSignature
  Squiz.ControlStructures.ForEachLoopDeclaration
  Squiz.ControlStructures.ForLoopDeclaration
  Squiz.ControlStructures.LowercaseDeclaration
  Squiz.Functions.FunctionDeclaration
  Squiz.Functions.FunctionDeclarationArgumentSpacing
  Squiz.Functions.LowercaseFunctionKeywords
  Squiz.WhiteSpace.CastSpacing
  Squiz.WhiteSpace.ControlStructureSpacing
  Squiz.WhiteSpace.ObjectOperatorSpacing
  Squiz.WhiteSpace.ScopeClosingBrace
  Squiz.WhiteSpace.SemicolonSpacing
  Squiz.WhiteSpace.SuperfluousWhitespace
```


## Install
Required composer.

Install PHP_CodeSniffer system-wide: 
````
composer global require "squizlabs/php_codesniffer=*"
````

Copy/clone this repository content to `/global_composer_vendor/squizlabs/php_codesniffer/src/Standards/Dsn`


## Getting started
`phpcs --standard=Dsn /path/to/file-or-directory`

If PHP_CodeSniffer finds any coding standard errors, a report will be shown after running the command.

Full usage information and example reports are available on the https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage.


List of Installed Coding Standards
`phpcs -i`

Not use an external standard by specifying the full path to the standard's root directory
`phpcs --standard=/path/to/MyStandard /path/to/code/myfile.inc`

Change the default standard
`phpcs --config-set default_standard PSR12`

To view the currently set configuration options
`phpcs --config-show`

To delete a configuration option
`phpcs --config-delete <option>`
